import { StatusBar } from 'expo-status-bar';
import { useState } from 'react';
import { ScrollView, StyleSheet, Text, View,FlatList, TouchableWithoutFeedback ,Keyboard} from 'react-native';
import Header from './components/Header';
import Form from './components/Form';
import List from './components/List'
import { AntDesign } from '@expo/vector-icons';
export default function App() {
  const [arr,setArr]= useState([
    
  ])
  
  return (
    <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
    <View style={styles.container}>
      <View style={styles.contain}>

        <View  style={styles.header}>
          <Header></Header>
          
        </View>

        <View style={styles.form}>
          <Form arr={arr} setArr={setArr} ></Form>
        </View>
       
        
        
        
      </View>
      <View style={styles.listed}>
        <View style={styles.ListSection}>
            <Text style={styles.text}>List Down Your Todo's</Text>
            <AntDesign name="down" size={18} color="white" />
          </View>
        <View style={styles.list}>
            <List arr={arr}></List>
          </View>
        </View>
    </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    
    backgroundColor: '#B84D4F',
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  contain:
  {
    backgroundColor:"#110D24",
    height:300,
    width:300,
    marginTop:0,
    padding:10,
    

  },
  
  text:{
    color:"#fff",
    paddingRight:8,
  },
  list:
  {
    flex:1,
    alignItems:"center",
    justifyContent:"center",
    padding:5,
    margin:8,

  },
  listed:
  {
    
    height:300,
    margin:5,
    alignItems:'center'
  },
  ListSection:
  {
    flex:1,
    flexDirection:"row"
  }
});
