import { ScrollView, StyleSheet, Text, View,FlatList, TouchableOpacity } from 'react-native';

const List = (props) => {
    const styles = StyleSheet.create({
        text:
        {
            color:"#fff",
            fontSize:15,
            width:200,
            borderLeftColor:"#110D24",
            borderBottomColor:"#1D1F63",
            borderBottomWidth:2,
            padding:10,
            margin:10,
            letterSpacing:2,
            
            
        },
        list:
        {
            
            width:200,
            
            
        }
    })
    return ( <FlatList key={props.arr.key}   style={styles.list}
        data={props.arr} renderItem={
          ({item})=>(
              <TouchableOpacity>
            <Text style={styles.text}>
            
                {item.key+" "}
                {item.name.toUpperCase()}</Text>
                </TouchableOpacity>
          )
        }></FlatList> );
}
 
export default List;