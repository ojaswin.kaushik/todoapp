import { Alert,TextInput,ScrollView, StyleSheet, Text, View,FlatList, Button } from 'react-native';
import { useState } from 'react';
const Form = (props) => {
    const styles= StyleSheet.create({
        Form:
        {
            backgroundColor:'#fff',
            margin:10,
            padding:10,
            borderRadius:5,
        },
        btn:
        {
            width:120,
            padding:10,
        },
        buttons:
        {
            flexDirection:"row"
        }
       
    })
    const [todo,setTodo]=useState('')
    function Add()
    {
       if (todo.split(' ').join('').length)
       {
        //const key_=Number(props.arr[-1]['key'])+1
        const key_=props.arr.length+1
        
        props.setArr((prev)=>{
            return [...prev,{name:todo,key:key_}]
        })
        //console.log(props.arr)
        setTodo('')
       }
       else{
           Alert.alert('Error','Todos should not be empty',[
               {
                   text:'Understood'
               }
           ])
       }
    }

    function del()
    {
        if (props.arr.length)
        {
            const key = props.arr.length
            
            props.setArr((prev)=>
            {
                return prev.filter((e)=>e.key!==key)
            })
            console.log(props.arr)
            
        }
    }
    return ( <View>
            <TextInput placeholder='What to add now!!!' 
            style={styles.Form}   value ={todo} onChangeText={(e)=>setTodo(e)}>


            </TextInput>
            <View style={styles.buttons}>
                <View style={styles.btn}>
                    <Button title='Add Todo' onPress={Add}></Button>
                
                </View>
                <View style={styles.btn}>
                
                    <Button title='Delete' onPress={del}></Button>
                </View>
            </View>
        </View> );
}
 
export default Form;