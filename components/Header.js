import { ScrollView, StyleSheet, Text, View,FlatList } from 'react-native';



const Header = () => {
    const styles=StyleSheet.create(
        {
            text:
            {
                color:"#fff",
                fontSize:30,
                margin:10,
                padding:10,

                width:200
            }
        }
    )
    return ( <Text style={styles.text}>Simple ToDo Application</Text> );
}
 
export default Header;